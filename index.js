const port = process.env.PORT || 8889
const logger = require('./src/helpers/log-driver');
const server = require('./src/server/api');

process.on('uncaughtException', ()=> {
  logger.error(err);
  process.exit(1);
});

server.listen(port, (err) => {
  if (err) throw err;
  logger.info(`Listening on http://localhost:${port}`, { event: 'launch'})
});
