const test = require('tape');
const request = require('supertest');
const nock = require('nock');

const service = require('../../server/api');


test('GET /status', (t) => {
  request(service)
    .get('/status')
    .expect(200)
    .end((err,res) => {
      t.error(err)
      t.equal(res.text, 'Nurse Charm is OK!')
      t.end();
    })
})
