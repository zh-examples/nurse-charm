const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

before((done)=>{
  mongoose.connect('mongodb://localhost/student_test');
  mongoose.connection
    .once('open',()=> {done();})
    .on('error', (error)=> {
      console.log('error => ', error);
    });
});

beforeEach((done) => {
  mongoose.connection.collections.students.drop(()=> {
    done();
  });
});

after(() => { mongoose.connection.close() })
