const assert = require('assert');
const Student = require('../../models/student');

describe('[STUDENT] create a record', ()=> {
  it('Saves a Student', (done) => {
    const janeDoe = new Student ({ firstName: 'Jane', lastName: 'Doe'});

    janeDoe.save().then(() => {
      assert(!janeDoe.isNew);
      done();
    });
  });
});


//https://stackoverflow.com/questions/18452023/installing-and-running-mongodb-on-osx
