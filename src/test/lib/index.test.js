const helper = require('../helpers/test-bot');
const dialog = require('../../lib/dialog');

describe('[ NurseCharm ] init', function() { // far arrows break mocha
  let bot;
  let botTester;
  this.timeout(15000);

  beforeEach(()=> {
    bot = helper.testBot(helper.connector, {
      '/': dialog
    })
    botTester = new helper.botTester.BotTester(bot)
  });

  it('[ NurseCharm ] says hello Zuri', ()=>  {
    return botTester
      .sendMessageToBot('Get Started', 'Hello! My name is Nurse Charm. What is your name?')
      .sendMessageToBot('Zuri', 'Hello! Zuri Nice to meet you! I have to go! Good Bye!')
      .runTest();
  })
})
