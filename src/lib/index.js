const botBuilder = require('botbuilder');
const start = require('./start.js');

module.exports = (connector) => {
  const nurseCharm = new builder.UniversalBot(connector).set('storage', inMemoryStorage);
  const inMemoryStorage = new builder.MemoryBotStorage();

  nurseCharm.('/', start);

  return nurseCharm;
}
