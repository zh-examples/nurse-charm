const mongoose = require('mongoose');
// mongoose.connect('http://localhost:4661') //suppose to be connecting to mongodb

const Schema = mongoose.Schema;

const StudentSchema = new Schema({
  studentId: String,
  photoId: String,
  name: {
    first: String,
    last: String
  },
  age: Number,
  guardians: [
    {
      name: {
        first: String,
        last: String,
        relationship: String,
        email: String,
        phoneNumber: String
      }
    }
  ],
  location: {
    address: String,
    city: String,
    state: String,
    zipCode: String
  },
  allergies: Array,
  prescribedMedicines: [
      {
      name: String,
      frequency: String,
      treatment: String
    }
  ],
  visits: [
    {
      date: Date,
      time: String,
      reasonOfVisit: String,
      healthReadings: {
        respiratoryRate: String,
        pulseOximetry: String,
        bloodGlucose: String
      },
      treatmentForVisit: String
    }
  ],
  screens: {
    vision: Boolean,
    hearing: Boolean
  }
});

module.exports = mongoose.model('Student', StudentSchema);
