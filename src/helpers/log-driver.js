const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');

const fs = require('fs');
const path = require('path');

const env = process.env.NODE_ENV || 'development';
const logDir = 'logs';
const filename = path.join(logDir, 'server-logs.log');
const rotateFileTransport = new transports.DailyRotateFile({
  filename: `${logDir}/%DATE%-results.log`,
  datePattern: 'YYYY-MM-DD'
});

if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

const logger = createLogger({
  level: env === 'development' ? 'debug' : 'info',
  format: format.combine(format.simple(),format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }, format.json()),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)),
  transports: [ new transports.Console({
      level: 'info',
      format: format.combine(
        format.colorize(),
        format.printf(
          info => `${info.timestamp} ${info.level}: ${info.message}`
        )
      )
    }),
    rotateFileTransport
  ]
});


module.exports = logger;
